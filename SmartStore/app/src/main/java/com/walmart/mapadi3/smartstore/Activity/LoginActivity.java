package com.walmart.mapadi3.smartstore.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.walmart.mapadi3.smartstore.Listener.RequestListener;
import com.walmart.mapadi3.smartstore.MainActivity;
import com.walmart.mapadi3.smartstore.Model.Product;
import com.walmart.mapadi3.smartstore.Model.User;
import com.walmart.mapadi3.smartstore.R;
import com.walmart.mapadi3.smartstore.Utils.Constants;
import com.walmart.mapadi3.smartstore.Utils.GeneralSharedPreferences;
import com.walmart.mapadi3.smartstore.WebRequest.ProductsWebRequest;
import com.walmart.mapadi3.smartstore.WebRequest.UsersWebRequest;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private EditText etUser;
    private EditText etPass;
    private Button btnlogin;
    private ProgressDialog pDialog;
    private Context context;
    private GeneralSharedPreferences generalSharedPreferences;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUser = (EditText) findViewById(R.id.user_login);
        etPass = (EditText) findViewById(R.id.pass_login);
        btnlogin = (Button) findViewById(R.id.login_button);

        context = this;

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLogin();
            }
        });
    }

    private boolean checkLogin(){

        String username = etUser.getText().toString();
        String password = etPass.getText().toString();

        hideKeyboard();

        //Validate login
        makeLoginRequest(username, password);

        return true;
    }

    private void hideKeyboard(){
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        if(getCurrentFocus() != null){

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);

        }
    }

    private void makeLoginRequest(String username, String password) {
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Login in");
        pDialog.setMessage("Wait a moment...");
        pDialog.show();

        Map<String, String> params = new HashMap<String, String>();
        params.put("username", username);
        params.put("password", password);
        UsersWebRequest.makePostUserRequest(Constants.URL_WEBSERVICES + "/users/", params, new RequestListener() {

            @Override
            public void onResponse(Object response) {
                generalSharedPreferences = new GeneralSharedPreferences(context, GeneralSharedPreferences.USER_PREFERENCES);

                pDialog.hide();
                pDialog.dismiss();
                Log.i("Volley request", "Success");
                Log.i("Response: ", "" + response);
                user = (User) response;

                generalSharedPreferences.write(GeneralSharedPreferences.USER_USERNAME, user.getUsername());
                generalSharedPreferences.write(GeneralSharedPreferences.USER_PASS, user.getPassword());
                generalSharedPreferences.write(GeneralSharedPreferences.USER_NAME, user.getName());
                generalSharedPreferences.write(GeneralSharedPreferences.USER_IMAGE, user.getImage());

                Intent intent;
                intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }

            @Override
            public void onError(String message) {
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });

    }

}
