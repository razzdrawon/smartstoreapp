package com.walmart.mapadi3.smartstore;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.walmart.mapadi3.smartstore.Activity.LoginActivity;
import com.walmart.mapadi3.smartstore.Activity.SplashActivity;
import com.walmart.mapadi3.smartstore.Fragment.ProductsFragment;
import com.walmart.mapadi3.smartstore.Fragment.RecipesFragment;
import com.walmart.mapadi3.smartstore.Fragment.ScanFragment;
import com.walmart.mapadi3.smartstore.Utils.GeneralSharedPreferences;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private GeneralSharedPreferences generalSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        generalSharedPreferences = new GeneralSharedPreferences(this, GeneralSharedPreferences.USER_PREFERENCES);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        loadNavigationView();

        //add this line to display products fragment when the activity is loaded
        changeFragment(R.id.nav_products);
    }

    private void loadNavigationView() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        CircleImageView circleImageView = (CircleImageView) navigationView.getHeaderView(0).findViewById(R.id.circleImageView);
        String image = generalSharedPreferences.read(GeneralSharedPreferences.USER_IMAGE);
        Glide.with(this).load(image).into(circleImageView);

        TextView tvUserName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.userName);
        tvUserName.setText(generalSharedPreferences.read(GeneralSharedPreferences.USER_USERNAME));

        TextView tvName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.name);
        tvName.setText(generalSharedPreferences.read(GeneralSharedPreferences.USER_NAME));

        Button btnLogOut = (Button) navigationView.getHeaderView(0).findViewById(R.id.btnLogOut);
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logOut();
            }
        });
    }

    private void logOut() {
        generalSharedPreferences.removeAll();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        changeFragment(item.getItemId());
        return true;
    }

    /**
     * Change to the indicated fragment
     */
    private void changeFragment(int id) {
        Fragment fragment = null;

        // Handle navigation view item clicks here.
        if (id == R.id.nav_products) {
            fragment = new ProductsFragment();
        } else if (id == R.id.nav_receipts) {
            fragment = new RecipesFragment();
        } else if (id == R.id.nav_scan) {
            fragment = new ScanFragment();
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_main, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

}
