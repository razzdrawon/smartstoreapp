package com.walmart.mapadi3.smartstore.Parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.walmart.mapadi3.smartstore.Model.Ingredient;
import com.walmart.mapadi3.smartstore.Model.Product;
import com.walmart.mapadi3.smartstore.Model.Recipe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by iia0006 on 27/02/17.
 */

public class RecipeParser {

    public static List<Recipe> parseRecipes(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<Recipe> recipes = new ArrayList<Recipe>();
        JsonNode rootNode = objectMapper.readTree(json);
        for(JsonNode dataAuxNode : rootNode) {
            recipes.add(objectMapper.treeToValue(dataAuxNode, Recipe.class));
        }
        return recipes;
    }

    public static Recipe parseRecipe(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Recipe recipe;
        List<Ingredient> ingredients = new ArrayList<Ingredient>();
        JsonNode rootNode = objectMapper.readTree(json);
        JsonNode recipeNode = rootNode.path("recipe");
        recipe = (objectMapper.treeToValue(recipeNode, Recipe.class));
        JsonNode ingredientsNode = rootNode.path("ingredients");
        if(ingredientsNode != null) {
            for(JsonNode ingredientAux: ingredientsNode){
                ingredients.add(objectMapper.treeToValue(ingredientAux, Ingredient.class));
            }
            recipe.setIngredients(ingredients);
        }
        return recipe;

    }
}
