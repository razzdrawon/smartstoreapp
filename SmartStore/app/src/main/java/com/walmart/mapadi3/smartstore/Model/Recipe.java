package com.walmart.mapadi3.smartstore.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

/**
 * Created by iia0006 on 27/02/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Recipe implements Serializable {

    private Integer idRecipes;
    private String title;
    private String image;
    private String preparation;
    private List<Ingredient> ingredients;

    public Recipe() {
    }

    public Recipe(Integer idRecipes, String title, String image, String preparation) {
        this.idRecipes = idRecipes;
        this.title = title;
        this.image = image;
        this.preparation = preparation;
    }

    public Integer getIdRecipes() {
        return idRecipes;
    }

    public void setIdRecipes(Integer idRecipes) {
        this.idRecipes = idRecipes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "idRecipes=" + idRecipes +
                ", title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", preparation='" + preparation + '\'' ;
    }
}
