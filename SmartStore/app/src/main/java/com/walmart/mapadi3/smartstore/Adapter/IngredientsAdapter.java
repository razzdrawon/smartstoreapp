package com.walmart.mapadi3.smartstore.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.walmart.mapadi3.smartstore.Model.Ingredient;
import com.walmart.mapadi3.smartstore.R;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by mapadi3 on 28/02/17.
 */

public class IngredientsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<Ingredient> mIngredients;
    IngredientsAdapter.OnItemClickListener mItemClickListener;

    public IngredientsAdapter(Context context, List<Ingredient> ingredients) {
        this.mContext = context;
        this.mIngredients = ingredients;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ingredients, parent, false);
        IngredientsAdapter.IngredientsViewHolder vh = new IngredientsAdapter.IngredientsViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, int position) {
        final Ingredient ingredient = mIngredients.get(position);
        final IngredientsAdapter.IngredientsViewHolder holder = (IngredientsAdapter.IngredientsViewHolder) holder1;

        holder.ingredientName.setText(ingredient.getName());
        holder.ingredientQuantity.setText(ingredient.getQuantity());


        if(ingredient.getProduct() != null) {
            holder.ingredientPrice.setText("$ " + ingredient.getProduct().getPrice().toString());
            Glide.with(mContext).load(ingredient.getProduct().getImage()).into(holder.ingredientImage);
            holder.ingredientHolder.setBackgroundColor(Color.WHITE);
        }
        else {
            holder.ingredientImage.setImageResource(R.drawable.ingredient);
        }
    }

    @Override
    public int getItemCount() {
        return mIngredients.size();
    }

    // 3
    public class IngredientsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public RelativeLayout ingredientHolder;
        public TextView ingredientQuantity;
        public TextView ingredientName;
        public TextView ingredientPrice;
        public ImageView ingredientImage;


        public IngredientsViewHolder(View itemView) {
            super(itemView);
            itemView.setClickable(true);
            ingredientHolder = (RelativeLayout) itemView.findViewById(R.id.ingredientHolder);
            ingredientQuantity = (TextView) itemView.findViewById(R.id.ingredientQuantity);
            ingredientName = (TextView) itemView.findViewById(R.id.ingredientName);
            ingredientPrice = (TextView) itemView.findViewById(R.id.ingredientPrice);
            ingredientImage = (ImageView) itemView.findViewById(R.id.imgIngredient);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.onItemClick(itemView, getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final IngredientsAdapter.OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public Ingredient getItem(int position) {
        return mIngredients.get(position);
    }

}
