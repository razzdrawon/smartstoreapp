package com.walmart.mapadi3.smartstore.Model;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by mapadi3 on 24/02/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product implements Serializable {
    private Integer idProduct;
    private String name;
    private String brand;
    private Double price;
    private String description;
    private String image;
    private String sku;
    private Double shelfId;
    private Double disccount;
    private Integer sponsoredPriority;
    private ProductType idProductType;
    private Store idStore;
    private ArrayList<Recipe> recipes;

    public Product() {
    }

    public Product(Integer idProduct, String name, String brand, Double price, String description, String image, String sku, Double shelfId, Double discount, Integer sponsoredPriority) {
        this.idProduct = idProduct;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.description = description;
        this.image = image;
        this.sku = sku;
        this.shelfId = shelfId;
        this.disccount = discount;
        this.sponsoredPriority = sponsoredPriority;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public Double getShelfId() {
        return shelfId;
    }

    public void setShelfId(Double shelfId) {
        this.shelfId = shelfId;
    }

    public Double getDisccount() {
        return disccount;
    }

    public void setDisccount(Double disccount) {
        this.disccount = disccount;
    }

    public Integer getSponsoredPriority() {
        return sponsoredPriority;
    }

    public void setSponsoredPriority(Integer sponsoredPriority) {
        this.sponsoredPriority = sponsoredPriority;
    }

    public ProductType getIdProductType() {
        return idProductType;
    }

    public void setIdProductType(ProductType idProductType) {
        this.idProductType = idProductType;
    }

    public Store getIdStore() {
        return idStore;
    }

    public void setIdStore(Store idStore) {
        this.idStore = idStore;
    }

    public ArrayList<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(ArrayList<Recipe> recipes) {
        this.recipes = recipes;
    }
}
