package com.walmart.mapadi3.smartstore.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.walmart.mapadi3.smartstore.Model.Recipe;
import com.walmart.mapadi3.smartstore.R;

import java.util.List;

/**
 * Created by mapadi3 on 28/02/17.
 */

public class RecipesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<Recipe> mRecipes;
    RecipesListAdapter.OnItemClickListener mItemClickListener;

    public RecipesListAdapter(Context context, List<Recipe> recipes) {
        this.mContext = context;
        this.mRecipes = recipes;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_recipes, parent, false);
        RecipesViewHolder vh = new RecipesViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, int position) {
        final Recipe recipe = mRecipes.get(position);
        final RecipesViewHolder holder = (RecipesViewHolder) holder1;

        holder.recipeTitle.setText(recipe.getTitle());
        holder.recipePreparation.setText(recipe.getPreparation());
        Glide.with(mContext).load(recipe.getImage()).into(holder.recipeImage);

    }

    @Override
    public int getItemCount() {
        return mRecipes.size();
    }

    // 3
    public class RecipesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public LinearLayout recipeHolder;
        public TextView recipeTitle;
        public TextView recipePreparation;
        public ImageView recipeImage;


        public RecipesViewHolder(View itemView) {
            super(itemView);
            itemView.setClickable(true);
            recipeHolder = (LinearLayout) itemView.findViewById(R.id.recipeNameHolder);
            recipeTitle = (TextView) itemView.findViewById(R.id.recipeTitle);
            recipePreparation = (TextView) itemView.findViewById(R.id.recipePreparation);
            recipeImage = (ImageView) itemView.findViewById(R.id.recipeImage);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.onItemClick(itemView, getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public Recipe getItem(int position) {
        return mRecipes.get(position);
    }

}
