package com.walmart.mapadi3.smartstore.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by macama3 on 28/02/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Store implements Serializable {

    private Integer idStore;
    private String name;

    public Store(){

    }

    public Store(Integer idStore, String name) {
        this.idStore = idStore;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdStore() {
        return idStore;
    }

    public void setIdStore(Integer idStore) {
        this.idStore = idStore;
    }
}
