package com.walmart.mapadi3.smartstore.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.walmart.mapadi3.smartstore.MainActivity;
import com.walmart.mapadi3.smartstore.R;
import com.walmart.mapadi3.smartstore.Utils.GeneralSharedPreferences;

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_DELAY = 2000;
    private GeneralSharedPreferences generalSharedPreferences;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;
        showMainActivity();
    }

    private void showMainActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                generalSharedPreferences = new GeneralSharedPreferences(context, GeneralSharedPreferences.USER_PREFERENCES);
                Intent intent;
                if(!existUserPreferences()) {
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                } else {
                    intent = new Intent(SplashActivity.this, MainActivity.class);
                }
                startActivity(intent);
                finish();
            }
        }, SPLASH_DELAY);
    }

    private boolean existUserPreferences() {
        String username = generalSharedPreferences.read(GeneralSharedPreferences.USER_USERNAME);
        String password = generalSharedPreferences.read(GeneralSharedPreferences.USER_PASS);
        String name = generalSharedPreferences.read(GeneralSharedPreferences.USER_NAME);
        String image = generalSharedPreferences.read(GeneralSharedPreferences.USER_IMAGE);
        return username != null && password != null && name != null && image != null;
    }

}
