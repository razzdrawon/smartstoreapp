package com.walmart.mapadi3.smartstore.Utils;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/**
 * Created by maagapi on 27/02/17.
 */

public class NfcOperations {

    private Tag tag;


    public NfcOperations(Tag tag) {
       this.tag=tag;
    }



    public String getInformation() throws WalmartAppException, UnsupportedEncodingException {
        Ndef ndef = Ndef.get(tag);
        if (ndef == null) {
            throw new WalmartAppException("Invalid tag");
        }
        NdefMessage ndefMessage = ndef.getCachedNdefMessage();
        NdefRecord[] records = ndefMessage.getRecords();
        for (NdefRecord ndefRecord : records) {
            byte[] payload = ndefRecord.getPayload();
            return new String(payload);
        }
        throw new WalmartAppException("Invalid tag");
    }


}
