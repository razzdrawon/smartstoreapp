package com.walmart.mapadi3.smartstore.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.walmart.mapadi3.smartstore.Adapter.RecipesListAdapter;
import com.walmart.mapadi3.smartstore.Adapter.RelatedProductsListAdapter;
import com.walmart.mapadi3.smartstore.Fragment.RelatedProductsFragment;
import com.walmart.mapadi3.smartstore.Listener.RequestListener;
import com.walmart.mapadi3.smartstore.Model.Product;
import com.walmart.mapadi3.smartstore.Model.Recipe;
import com.walmart.mapadi3.smartstore.R;
import com.walmart.mapadi3.smartstore.Utils.Constants;
import com.walmart.mapadi3.smartstore.Utils.NfcOperations;
import com.walmart.mapadi3.smartstore.Utils.WalmartAppException;
import com.walmart.mapadi3.smartstore.WebRequest.ProductsWebRequest;

import java.io.UnsupportedEncodingException;
import java.util.List;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.widget.Toast;

public class ProductDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressDialog pDialog;
    private Context context;

    private ImageView mImageView;
    private TextView mBrand;
    private TextView mTitle;
    private TextView mPrice;
    private TextView mDescription;
    private TextView mType;

    private FrameLayout mDisccountHolder;
    private TextView mDisccount;

    private LinearLayout mTitleHolder;

    private LinearLayout mRecipesHolder;
    private LinearLayout mRelatedHolder;

    private ImageButton mAddButton;

    private Bitmap photo;
    private Product product;
    private int defaultColor;

    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerViewRelated;

    private RecipesListAdapter mRecipesAdapter;
    private RelatedProductsListAdapter mRelatedProductsListAdapter;

    private StaggeredGridLayoutManager mStaggeredLayoutManager;
    private List<Recipe> recipes;
    private List<Product> products;

    public static final String MIME_TEXT_PLAIN = "walmart/smartstore";

    private Tag getTag(Intent intent) throws WalmartAppException {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            String type = intent.getType();
            if (MIME_TEXT_PLAIN.equals(type)) {
                return intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            } else {
                throw new WalmartAppException("Wrong mime type: " + type);
            }
        } else{
            throw new WalmartAppException("Wrong mime type" );
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        context = this;

        mImageView = (ImageView) findViewById(R.id.productImage);
        mBrand = (TextView) findViewById(R.id.tvBrand);
        mTitle = (TextView) findViewById(R.id.tvProductName);
        mPrice = (TextView) findViewById(R.id.tvPriceNum);
        mDescription = (TextView) findViewById(R.id.tvDescription);
        mType = (TextView) findViewById(R.id.tvType);
        mDisccount = (TextView) findViewById(R.id.tvDiscount);
        mTitleHolder = (LinearLayout) findViewById(R.id.productNameHolder);

        mRecipesHolder = (LinearLayout) findViewById(R.id.recipesContainer);
        mRelatedHolder = (LinearLayout) findViewById(R.id.relatedProductsContainer);

        mDisccountHolder = (FrameLayout) findViewById(R.id.discountHolder);
        mAddButton = (ImageButton) findViewById(R.id.btn_add);

        mRecyclerView = (RecyclerView) findViewById(R.id.listRecipes);
        mRecyclerViewRelated = (RecyclerView) findViewById(R.id.listRelatedProducts);
        try {
            Intent intent = getIntent();
            String information = new NfcOperations(getTag(getIntent())).getInformation();
            makeGetProductsRequest(information,null);
        } catch (WalmartAppException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        Product prod;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                prod = null;
            } else {
                prod = (Product) extras.getSerializable("PRODUCT");
            }
        } else {
            prod = (Product) savedInstanceState.getSerializable("PRODUCT");
        }


        if(prod != null)
        makeGetProductsRequest(prod.getIdProduct().toString(), prod.getIdProductType().getIdProductType().toString());


        mAddButton.setOnClickListener(this);
        defaultColor = getResources().getColor(R.color.colorPrimaryDark);


    }

    private void makeGetProductsRequest(String idProd,String idType) {
        pDialog = new ProgressDialog(context);
        pDialog.setTitle("Getting product details");
        pDialog.setMessage("Wait...");
        pDialog.show();

        ProductsWebRequest.makeGetProductRequest(Constants.URL_WEBSERVICES + "/products/" + idProd, new RequestListener() {

            @Override
            public void onResponse(Object response) {
                pDialog.hide();
                pDialog.dismiss();
                Log.i("Volley request", "Success");
                Log.i("*response", "" + response);
                product = (Product) response;

                loadProduct();
                windowTransition();
                getPhoto(product.getImage());
            }

            @Override
            public void onError(String message) {
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(context, "An error connection occurred.", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void loadProduct() {
        mBrand.setText(product.getBrand());
        mTitle.setText(product.getName());
        mPrice.setText(product.getPrice().toString());
        mDescription.setText(product.getDescription());
        mType.setText(product.getIdProductType().getName());
        mDescription.setText(product.getDescription());
        Glide.with(this).load(product.getImage()).into(mImageView);

        if(product.getDisccount() != null && product.getDisccount() > 0 ){
            mDisccountHolder.setVisibility(View.VISIBLE);
            mDisccount.setText(product.getDisccount().toString());
        }


        recipes = product.getRecipes();

        if(recipes != null && recipes.size() > 0){
            mRecyclerView.setAdapter(getListAdapter());
            setRecyclerViewOnClickListener();
        }
        else{
            mRecipesHolder.setVisibility(View.GONE);
        }

        makeGetRelatedProductsRequest(product.getIdProductType().getIdProductType().toString());

    }

    private void windowTransition() {

    }

    private void getPhoto(String url) {
        Picasso.with(this)
                .load(url)
                .into(mImageView, new com.squareup.picasso.Callback() {

                    @Override public void onSuccess() {
                        photo = ((BitmapDrawable)mImageView.getDrawable()).getBitmap();
                        Palette mPalette = Palette.generate(photo);
                        applyPalette(mPalette);
                    }

                    @Override public void onError() {
                        // reset your views to default colors, etc.
                    }

                });
    }

    private void applyPalette(Palette mPalette) {
        getWindow().setBackgroundDrawable(new ColorDrawable(mPalette.getLightMutedColor(defaultColor)));
        mTitleHolder.setBackgroundColor(mPalette.getMutedColor(defaultColor));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:

        }
    }

    @Override
    public void onBackPressed() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(100);
        mAddButton.startAnimation(alphaAnimation);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mAddButton.setVisibility(View.GONE);
                finishAfterTransition();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private RecipesListAdapter getListAdapter(){
        mRecipesAdapter = new RecipesListAdapter(ProductDetailsActivity.this, recipes);
        return mRecipesAdapter;
    }

    private void makeGetRelatedProductsRequest(String typeID){
        ProductsWebRequest.makeGetProductsRequest(Constants.URL_WEBSERVICES.concat("/products/type/").concat(typeID).concat("/id"), new RequestListener() {

            @Override
            public void onResponse(Object response) {

                pDialog.hide();
                pDialog.dismiss();
                Log.i("Volley request", "Success");
                Log.i("*response", "" + response);
                products = (List<Product>) response;

                loadRelatedProducts();

            }


            @Override
            public void onError(String message) {
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(context, "An error connection occurred.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadRelatedProducts() {
        if(products != null && products.size() > 0){
            mRecyclerViewRelated.setAdapter(getRelatedListAdapter());
            setRelatedRecyclerViewOnClickListener();
        }
        else{
            mRecipesHolder.setVisibility(View.GONE);
        }

    }

    private RelatedProductsListAdapter getRelatedListAdapter(){
        mRelatedProductsListAdapter = new RelatedProductsListAdapter(ProductDetailsActivity.this, products);
        return mRelatedProductsListAdapter;
    }

    private void setRecyclerViewOnClickListener(){
        mRecipesAdapter.setOnItemClickListener(new RecipesListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//                Toast.makeText(context, "Clicked " + position, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ProductDetailsActivity.this, RecipeActivity.class);
                intent.putExtra("RECIPE", mRecipesAdapter.getItem(position));
                startActivity(intent);
            }
        });
    }

    private void setRelatedRecyclerViewOnClickListener(){
        mRelatedProductsListAdapter.setOnItemClickListener(new RelatedProductsListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//                Toast.makeText(context, "Clicked " + position, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ProductDetailsActivity.this, ProductDetailsActivity.class);
                intent.putExtra("PRODUCT", mRelatedProductsListAdapter.getItem(position));
                startActivity(intent);
            }
        });
    }
}
