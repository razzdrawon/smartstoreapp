package com.walmart.mapadi3.smartstore.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by iia0006 on 28/02/17.
 */

public class GeneralSharedPreferences {

    public static final String USER_PREFERENCES = "UserInfo";

    public static final String USER_USERNAME = "username";
    public static final String USER_PASS = "password";
    public static final String USER_NAME = "name";
    public static final String USER_IMAGE = "image";

    private SharedPreferences sharedPreferences;

    public GeneralSharedPreferences(Context context, String preferenceFileKey) {
        this.sharedPreferences = context.getSharedPreferences(preferenceFileKey, Context.MODE_PRIVATE);
    }

    public void write(String key, String value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String read(String key){
        return sharedPreferences.getString(key, null);
    }

    public void removeAll(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

}
