package com.walmart.mapadi3.smartstore.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.walmart.mapadi3.smartstore.Model.Product;
import com.walmart.mapadi3.smartstore.R;

import java.net.URL;
import java.util.List;

/**
 * Created by mapadi3 on 24/02/17.
 */

public class ProductsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<Product> mProducts;
    OnItemClickListener mItemClickListener;


    public ProductsListAdapter(Context context, List<Product> products) {
        this.mContext = context;
        this.mProducts = products;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_products, parent, false);
        ProductsViewHolder vh = new ProductsViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, int position) {
        final Product product = mProducts.get(position);
        final ProductsViewHolder holder = (ProductsViewHolder) holder1;

        holder.placeName.setText(product.getName());

        Glide.with(mContext).load(product.getImage()).into(holder.placeImage);

        Picasso.with(mContext)
                .load(product.getImage())
                .into(holder.placeImage, new com.squareup.picasso.Callback() {

                    @Override public void onSuccess() {
                        Bitmap bitmap = ((BitmapDrawable)holder.placeImage.getDrawable()).getBitmap();
                        Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
                            public void onGenerated(Palette palette) {
                                int bgColor = palette.getMutedColor(mContext.getResources().getColor(R.color.colorAccent));
                                holder.placeNameHolder.setBackgroundColor(bgColor);
                            }
                        });
                        // do your processing here....
                    }

                    @Override public void onError() {
                        // reset your views to default colors, etc.
                    }

                });
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    // 3
    public class ProductsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public LinearLayout placeHolder;
        public LinearLayout placeNameHolder;
        public TextView placeName;
        public ImageView placeImage;


        public ProductsViewHolder(View itemView) {
            super(itemView);
            itemView.setClickable(true);
            placeHolder = (LinearLayout) itemView.findViewById(R.id.mainHolder);
            placeName = (TextView) itemView.findViewById(R.id.placeName);
            placeNameHolder = (LinearLayout) itemView.findViewById(R.id.placeNameHolder);
            placeImage = (ImageView) itemView.findViewById(R.id.placeImage);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mItemClickListener.onItemClick(itemView, getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public Product getItem(int position) {
        return mProducts.get(position);
    }

}
