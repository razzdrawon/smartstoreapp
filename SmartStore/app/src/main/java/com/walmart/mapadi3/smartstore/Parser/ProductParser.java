package com.walmart.mapadi3.smartstore.Parser;

import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.walmart.mapadi3.smartstore.Model.Product;
import com.walmart.mapadi3.smartstore.Model.Recipe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mapadi3 on 27/02/17.
 */

public class ProductParser {

    public static List<Product> parseProducts(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        List<Product> products = new ArrayList<Product>();
        JsonNode rootNode = objectMapper.readTree(json);
        for(JsonNode dataAuxNode : rootNode) {
                products.add(objectMapper.treeToValue(dataAuxNode, Product.class));
        }
        return products;
    }

    public static Product parseProduct(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Product product;
        List<Recipe> recipes = new ArrayList<Recipe>();
        JsonNode rootNode = objectMapper.readTree(json);
        JsonNode productNode = rootNode.path("product");
        product = (objectMapper.treeToValue(productNode, Product.class));
        JsonNode recipesNode = rootNode.path("recipes");
        if(recipesNode != null) {
            for(JsonNode recipeAux: recipesNode){
                recipes.add(objectMapper.treeToValue(recipeAux, Recipe.class));
            }
            product.setRecipes((ArrayList<Recipe>) recipes);
        }
        return product;

    }

}
