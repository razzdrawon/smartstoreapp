package com.walmart.mapadi3.smartstore.WebRequest;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.walmart.mapadi3.smartstore.Listener.RequestListener;
import com.walmart.mapadi3.smartstore.Model.Product;
import com.walmart.mapadi3.smartstore.Model.User;
import com.walmart.mapadi3.smartstore.Parser.ProductParser;
import com.walmart.mapadi3.smartstore.Parser.UserParser;
import com.walmart.mapadi3.smartstore.Utils.VolleySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by iia0006 on 28/02/17.
 */

public class UsersWebRequest {

    public static final String TAG_VOLLEY_REQ_EXCEPTION = "Volley Req Exception";

    public static void makePostUserRequest(final String url, final Map<String, String> params, final RequestListener listener)  {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),

                new Response.Listener(){

                    @Override
                    public void onResponse(Object response) {
                        User user= null;
                        try {
                            user = UserParser.parseUser(response.toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        listener.onResponse(user);
                    }

                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG_VOLLEY_REQ_EXCEPTION, error.toString());
                        if (error.networkResponse != null && error.networkResponse.statusCode == 401)
                            listener.onError("Authentication failed");
                        else
                            listener.onError("An error occurred.");
                    }
                });

        // Adding request to request queue
        VolleySingleton.getInstance().addToRequestQueue(jsonObjReq);
    }
}
