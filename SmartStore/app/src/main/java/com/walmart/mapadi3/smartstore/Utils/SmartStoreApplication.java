package com.walmart.mapadi3.smartstore.Utils;

import android.app.Application;

/**
 * Created by mapadi3 on 22/02/17.
 */

public class SmartStoreApplication extends Application {

    private static SmartStoreApplication smartStoreApplicationContext;

    public SmartStoreApplication() {
        this.smartStoreApplicationContext = this;
    }

    public static SmartStoreApplication getAppContext() {
        return smartStoreApplicationContext;
    }
}

