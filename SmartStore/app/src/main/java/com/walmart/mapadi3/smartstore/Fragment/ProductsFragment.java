package com.walmart.mapadi3.smartstore.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.walmart.mapadi3.smartstore.Activity.ProductDetailsActivity;
import com.walmart.mapadi3.smartstore.Adapter.ProductsListAdapter;
import com.walmart.mapadi3.smartstore.Listener.RequestListener;
import com.walmart.mapadi3.smartstore.Model.Product;
import com.walmart.mapadi3.smartstore.R;
import com.walmart.mapadi3.smartstore.Utils.Constants;
import com.walmart.mapadi3.smartstore.WebRequest.ProductsWebRequest;

import java.util.List;

/**
 * Created by mapadi3 on 24/02/17.
 */

public class ProductsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mStaggeredLayoutManager;
    List<Product> products;

    private ProgressDialog pDialog;
    private Context context;


    private ProductsListAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_products, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.listProducts);
        mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
//        mRecyclerView.setLayoutManager(mStaggeredLayoutManager);
//        mStaggeredLayoutManager.setSpanCount(2);
//        mRecyclerView.setHasFixedSize(true);

//        products = new ArrayList<Product>();
//        Product p1 = new Product();
//        p1.setName("Nescafe");
//        p1.setImage("http://3.bp.blogspot.com/-ITVl3RbgNk0/T4M-8xfo9II/AAAAAAAAAAw/mLj3s6NGqNM/s1600/nescafetaza.jpg");
//        products.add(p1);
//        Product p2 = new Product();
//        p2.setName("Kellogs");
//        p2.setImage("http://fundacion-antama.org/wp-content/uploads/2013/01/Kelloggs-biotecnologia.jpg");
//        products.add(p2);
//        Product p3 = new Product();
//        p3.setName("Wine");
//        p3.setImage("http://superhumancoach.com/wp-content/uploads/2014/07/red-wine.jpg");
//        products.add(p3);
//        Product p4 = new Product();
//        p4.setName("Lays");
//        p4.setImage("http://www.indofood.com/images/microsite/product/147lays-rasa-asin-klasik_small.jpg");
//        products.add(p4);

//        mAdapter = new ProductsListAdapter(getActivity(), products);
//        mRecyclerView.setAdapter(mAdapter);
//        setRecyclerViewOnClickListener();

        makeGetProductsRequest();




        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Products");
    }




    private void setRecyclerViewOnClickListener(){
        mAdapter.setOnItemClickListener(new ProductsListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//                Toast.makeText(getActivity(), "Clicked " + position, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
                intent.putExtra("PRODUCT", (Product) mAdapter.getItem(position));
                startActivity(intent);
            }
        });
    }


    private void makeGetProductsRequest() {
        pDialog = new ProgressDialog(getActivity());
        pDialog.setTitle("Getting products");
        pDialog.setMessage("Wait...");
        pDialog.show();

        ProductsWebRequest.makeGetProductsRequest(Constants.URL_WEBSERVICES + "/products", new RequestListener() {

                    @Override
                    public void onResponse(Object response) {

                        pDialog.hide();
                        pDialog.dismiss();
                        Log.i("Volley request", "Success");
                        Log.i("*response", "" + response);
                        products = (List<Product>) response;

                        mRecyclerView.setLayoutManager(mStaggeredLayoutManager);
                        mStaggeredLayoutManager.setSpanCount(2);
                        mRecyclerView.setAdapter(getListAdapter());
                        setRecyclerViewOnClickListener();

                    }


                    @Override
                    public void onError(String message) {
                        pDialog.hide();
                        pDialog.dismiss();
                            Toast.makeText(getActivity().getApplicationContext(), "An error connection occurred.", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private ProductsListAdapter getListAdapter(){
        mAdapter = new ProductsListAdapter(getActivity(), products);
        return mAdapter;
    }

}
