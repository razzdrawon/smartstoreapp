package com.walmart.mapadi3.smartstore.Utils;

/**
 * Created by maagapi on 27/02/17.
 */

public class WalmartAppException extends Exception {
    public WalmartAppException(String message) {
        super(message);
    }
}
