package com.walmart.mapadi3.smartstore.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by macama3 on 28/02/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductType implements Serializable {

    private Integer idProductType;
    private String name;

    public ProductType(){

    }

    public ProductType(Integer idProductType, String name) {
        this.idProductType = idProductType;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdProductType() {
        return idProductType;
    }

    public void setIdProductType(Integer idProductType) {
        this.idProductType = idProductType;
    }
}
