package com.walmart.mapadi3.smartstore.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by macama3 on 28/02/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Recipes implements Serializable{
    private Recipe recipe;
    private ArrayList<Ingredient> ingredients;

    public Recipes() {
    }

    public Recipes(Recipe recipe, ArrayList<Ingredient> ingredients) {
        this.recipe = recipe;
        this.ingredients = ingredients;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public ArrayList<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
