package com.walmart.mapadi3.smartstore.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.walmart.mapadi3.smartstore.Adapter.IngredientsAdapter;
import com.walmart.mapadi3.smartstore.Adapter.ProductsListAdapter;
import com.walmart.mapadi3.smartstore.Listener.RequestListener;
import com.walmart.mapadi3.smartstore.Model.Ingredient;
import com.walmart.mapadi3.smartstore.Model.Product;
import com.walmart.mapadi3.smartstore.Model.Recipe;
import com.walmart.mapadi3.smartstore.R;
import com.walmart.mapadi3.smartstore.Utils.Constants;
import com.walmart.mapadi3.smartstore.WebRequest.RecipesWebRequest;

public class RecipeActivity extends AppCompatActivity {

    private Context context;
    private ProgressDialog pDialog;

    private Bitmap photo;
    private Recipe recipe;
    private int defaultColor;

    private RecyclerView ingredientsList;
    private StaggeredGridLayoutManager mStaggeredLayoutManager;
    private IngredientsAdapter mAdapter;

    private ImageView ivRecipeImage;
    private TextView tvRecipeTitleDetails;
    private TextView tvRecipePreparation;
    private TextView totalPrice;
    private LinearLayout mTitleRecipeDetailsHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        ivRecipeImage = (ImageView) findViewById(R.id.ivRecipeImage);
        tvRecipeTitleDetails = (TextView) findViewById(R.id.tvRecipeDetailsName);
        mTitleRecipeDetailsHolder = (LinearLayout) findViewById(R.id.recipeDetailsNameHolder);
        tvRecipePreparation = (TextView) findViewById(R.id.tvRecipeTextPreparation);
        ingredientsList = (RecyclerView) findViewById(R.id.listIngredients);
        totalPrice= (TextView) findViewById(R.id.totalPrice);
        mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);


        Recipe rep;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                rep = null;
            } else {
                rep = (Recipe) extras.getSerializable("RECIPE");
            }
        } else {
            rep = (Recipe) savedInstanceState.getSerializable("RECIPE");
        }

        if(rep != null)
            makeGetRecipeRequest(rep.getIdRecipes().toString());
    }

    private void makeGetRecipeRequest(String idRecipe) {
        pDialog = new ProgressDialog(this);
        pDialog.setTitle("Getting recipe");
        pDialog.setMessage("Wait...");
        pDialog.show();

        RecipesWebRequest.makeGetRecipeRequest(Constants.URL_WEBSERVICES + "/recipes/" + idRecipe, new RequestListener() {

            @Override
            public void onResponse(Object response) {
                pDialog.hide();
                pDialog.dismiss();
                Log.i("Volley request", "Success");
                Log.i("*response", "" + response);
                recipe = (Recipe) response;

                loadRecipe();
                getPhoto(recipe.getImage());
//
                if(recipe.getIngredients() != null) {
                ingredientsList.setLayoutManager(mStaggeredLayoutManager);
                mStaggeredLayoutManager.setSpanCount(1);
                    ViewGroup.LayoutParams layoutParams = ingredientsList.getLayoutParams();
                    layoutParams.height=recipe.getIngredients().size()*200;
                    ingredientsList.setLayoutParams(layoutParams);
                    ingredientsList.setAdapter(getListAdapter());
                    setRecyclerViewOnClickListener();
                    double total=0;
                    for(Ingredient ing:recipe.getIngredients()){
                        if(ing.getProduct()!=null){
                            total+=ing.getProduct().getPrice();
                        }
                    }
                    totalPrice.setText("Total price: "+total);
                }
            }

            @Override
            public void onError(String message) {
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(context, "An error connection occurred.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadRecipe() {
        tvRecipeTitleDetails.setText(recipe.getTitle());
        tvRecipePreparation.setText(recipe.getPreparation());
        Glide.with(this).load(recipe.getImage()).into(ivRecipeImage);
    }


    private void getPhoto(String url) {
        Picasso.with(this)
                .load(url)
                .into(ivRecipeImage, new com.squareup.picasso.Callback() {

                    @Override
                    public void onSuccess() {
                        photo = ((BitmapDrawable) ivRecipeImage.getDrawable()).getBitmap();
                        Palette mPalette = Palette.generate(photo);
                        applyPalette(mPalette);
                    }

                    @Override
                    public void onError() {
                        // reset your views to default colors, etc.
                    }

                });
    }

    private void applyPalette(Palette mPalette) {
        getWindow().setBackgroundDrawable(new ColorDrawable(mPalette.getLightMutedColor(defaultColor)));
        mTitleRecipeDetailsHolder.setBackgroundColor(mPalette.getMutedColor(defaultColor));
    }

    private IngredientsAdapter getListAdapter(){
        mAdapter = new IngredientsAdapter(this, recipe.getIngredients());
        return mAdapter;
    }

    private void setRecyclerViewOnClickListener(){
        mAdapter.setOnItemClickListener(new IngredientsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//                Toast.makeText(RecipeActivity.this, "Clicked " + position, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(RecipeActivity.this, ProductDetailsActivity.class);
                intent.putExtra("PRODUCT", (Product) mAdapter.getItem(position).getProduct());
                startActivity(intent);
            }
        });
    }


}
