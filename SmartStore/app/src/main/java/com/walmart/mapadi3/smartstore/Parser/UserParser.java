package com.walmart.mapadi3.smartstore.Parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.walmart.mapadi3.smartstore.Model.Product;
import com.walmart.mapadi3.smartstore.Model.User;

import java.io.IOException;

/**
 * Created by iia0006 on 28/02/17.
 */

public class UserParser {

    public static User parseUser(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        User user = (objectMapper.readValue(json, User.class));
        return user;
    }
}
