package com.walmart.mapadi3.smartstore.Model;

/**
 * Created by iia0006 on 28/02/17.
 */

public class User {
    private Integer idUser;
    private String username;
    private String password;
    private String name;
    private String image;

    public User() {
    }

    public User(Integer idUser, String username, String password, String name, String image) {
        this.idUser = idUser;
        this.username = username;
        this.password = password;
        this.name = name;
        this.image = image;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "User{" +
                "idUser=" + idUser +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
