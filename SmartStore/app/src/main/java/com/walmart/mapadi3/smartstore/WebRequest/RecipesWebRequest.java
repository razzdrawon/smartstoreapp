package com.walmart.mapadi3.smartstore.WebRequest;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.walmart.mapadi3.smartstore.Listener.RequestListener;
import com.walmart.mapadi3.smartstore.Model.Product;
import com.walmart.mapadi3.smartstore.Model.Recipe;
import com.walmart.mapadi3.smartstore.Parser.ProductParser;
import com.walmart.mapadi3.smartstore.Parser.RecipeParser;
import com.walmart.mapadi3.smartstore.Utils.VolleySingleton;

import java.io.IOException;
import java.util.List;

/**
 * Created by iia0006 on 27/02/17.
 */

public class RecipesWebRequest {

    public static void makeGetRecipesRequest(final String url, final RequestListener listener)  {
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Request.Method.GET, url,

        new Response.Listener(){

            @Override
            public void onResponse(Object response) {
                List<Recipe> recipes= null;
                try {
                    recipes = RecipeParser.parseRecipes(response.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                listener.onResponse(recipes);
            }

        },
        new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy( // total timeout 20 segundos
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                //Timeout - Specifies Socket Timeout in millis per every retry attempt.
                1, //Number Of Retries - Number of times retry is attempted.
                3)); // Back Off Multiplier - A multiplier which is used to determine exponential time set to socket for every retry attempt.

        // Adding request to request queue
        VolleySingleton.getInstance().addToRequestQueue(jsonObjReq);
    }

    public static void makeGetRecipeRequest(final String url, final RequestListener listener)  {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url,

                new Response.Listener(){

                    @Override
                    public void onResponse(Object response) {
                        Recipe recipe= null;
                        try {
                            recipe = RecipeParser.parseRecipe(response.toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        listener.onResponse(recipe);
                    }

                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });


        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy( // total timeout 20 segundos
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                //Timeout - Specifies Socket Timeout in millis per every retry attempt.
                1, //Number Of Retries - Number of times retry is attempted.
                3)); // Back Off Multiplier - A multiplier which is used to determine exponential time set to socket for every retry attempt.

        // Adding request to request queue
        VolleySingleton.getInstance().addToRequestQueue(jsonObjReq);
    }
}
