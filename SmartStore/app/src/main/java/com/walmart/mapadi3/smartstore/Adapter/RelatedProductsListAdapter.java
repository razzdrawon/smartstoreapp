package com.walmart.mapadi3.smartstore.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.walmart.mapadi3.smartstore.Model.Product;
import com.walmart.mapadi3.smartstore.R;

import java.util.List;

/**
 * Created by macama3 on 28/02/17.
 */

public class RelatedProductsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<Product> mProducts;
    OnItemClickListener mItemClickListener;


    public RelatedProductsListAdapter(Context context, List<Product> products) {
        this.mContext = context;
        this.mProducts = products;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_related_products, parent, false);
        ProductsViewHolder vh = new ProductsViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, int position) {
        final Product product = mProducts.get(position);
        final ProductsViewHolder holder = (ProductsViewHolder) holder1;
        Glide.with(mContext).load(product.getImage()).into(holder.relatedImage);
        holder.relatedName.setText(product.getName());
        if(product.getDescription().length()>40)
            holder.relatedDescription.setText(product.getDescription().substring(0,40).concat("..."));
        else
            holder.relatedDescription.setText(product.getDescription());
        holder.relatedPrice.setText("Price:".concat(product.getPrice().toString()).concat("$"));
        if(product.getDisccount() != null){
        holder.relatedOffer.setText("Discount:".concat(product.getDisccount().toString()).concat("$"));
        } else {
            holder.relatedOffer.setText("-");
        }

    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    // 3
    public class ProductsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public LinearLayout relatedHolder;
        public LinearLayout relatedNameHolder;
        public TextView relatedName;
        public ImageView relatedImage;
        public TextView relatedOffer;
        public TextView relatedPrice;
        public TextView relatedDescription;

        public ProductsViewHolder(View itemView) {
            super(itemView);
            itemView.setClickable(true);
            relatedHolder = (LinearLayout) itemView.findViewById(R.id.mainRelatedHolder);
            relatedName = (TextView) itemView.findViewById(R.id.relatedName);
            relatedOffer = (TextView) itemView.findViewById(R.id.relatedOffer);
            relatedPrice = (TextView) itemView.findViewById(R.id.relatedPrice);
            relatedDescription = (TextView) itemView.findViewById(R.id.relatedDescription);
            relatedNameHolder = (LinearLayout) itemView.findViewById(R.id.relatedNameHolder);
            relatedImage = (ImageView) itemView.findViewById(R.id.relatedImage);
//            placeHolder.setOnClickListener(this);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            mItemClickListener.onItemClick(itemView, getAdapterPosition());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public Product getItem(int position) {
        return mProducts.get(position);
    }

}
