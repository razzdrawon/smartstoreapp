package com.walmart.mapadi3.smartstore.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.walmart.mapadi3.smartstore.Activity.ProductDetailsActivity;
import com.walmart.mapadi3.smartstore.Adapter.RelatedProductsListAdapter;
import com.walmart.mapadi3.smartstore.Listener.RequestListener;
import com.walmart.mapadi3.smartstore.Model.Product;
import com.walmart.mapadi3.smartstore.R;
import com.walmart.mapadi3.smartstore.Utils.Constants;
import com.walmart.mapadi3.smartstore.WebRequest.ProductsWebRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macama3 on 27/02/17.
 */

public class RelatedProductsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mStaggeredLayoutManager;
    List<Product> products;

    private ProgressDialog pDialog;
    private Context context;


    private RelatedProductsListAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        String typeID = getArguments().getString("typeID");
        View view = inflater.inflate(R.layout.fragment_related_products, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.listRelatedProducts);
        mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        makeGetRelatedProductsRequest(typeID);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Related Products");
    }




    private void setRecyclerViewOnClickListener(){
        mAdapter.setOnItemClickListener(new RelatedProductsListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Toast.makeText(getActivity(), "Clicked " + position, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
                intent.putExtra("PRODUCT", (Product) mAdapter.getItem(position));
//                startActivityForResult(intent, 1);
                startActivity(intent);
            }
        });
    }

    private void makeGetRelatedProductsRequest(String typeID) {
        pDialog = new ProgressDialog(getActivity());
        pDialog.setTitle("Getting products");
        pDialog.setMessage("Wait...");
        pDialog.show();
        Log.d("TEST","LOG");
        ProductsWebRequest.makeGetProductsRequest(Constants.URL_WEBSERVICES.concat("/products/type/").concat(typeID).concat("/id"), new RequestListener() {

            @Override
            public void onResponse(Object response) {

                pDialog.hide();
                pDialog.dismiss();
                Log.i("Volley request", "Success");
                Log.i("*response", "" + response);
                products = (List<Product>) response;

                mRecyclerView.setLayoutManager(mStaggeredLayoutManager);
                mStaggeredLayoutManager.setSpanCount(1);
                mRecyclerView.setAdapter(getListAdapter());
                setRecyclerViewOnClickListener();

            }


            @Override
            public void onError(String message) {
                pDialog.hide();
                pDialog.dismiss();
                Toast.makeText(getActivity().getApplicationContext(), "An error connection occurred.", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private RelatedProductsListAdapter getListAdapter(){
        mAdapter = new RelatedProductsListAdapter(getActivity(), products);
        return mAdapter;
    }

}
