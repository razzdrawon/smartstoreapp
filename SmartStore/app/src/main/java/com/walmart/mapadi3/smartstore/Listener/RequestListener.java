package com.walmart.mapadi3.smartstore.Listener;

/**
 * Created by Ricardo on 13/04/16.
 */
public interface RequestListener {
    public void onResponse(Object response);

    public void onError(String message);

}
