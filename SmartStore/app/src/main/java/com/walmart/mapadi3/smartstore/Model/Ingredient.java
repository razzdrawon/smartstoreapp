package com.walmart.mapadi3.smartstore.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by macama3 on 28/02/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Ingredient implements Serializable {
    private Integer idIngredient;
    private String name;
    private String quantity;
    private Product product;

    public Ingredient(){

    }

    public Ingredient(Integer idIngredient, String name, String quantity) {
        this.idIngredient = idIngredient;
        this.name = name;
        this.quantity = quantity;
    }

    public Integer getIdIngredient() {
        return idIngredient;
    }

    public void setIdIngredient(Integer idIngredient) {
        this.idIngredient = idIngredient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
